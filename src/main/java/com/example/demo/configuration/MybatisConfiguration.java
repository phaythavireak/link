package com.example.demo.configuration;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

@Configuration
@MapperScan("com.example.demo.repository")
public class MybatisConfiguration {


    public MybatisConfiguration(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public DataSource dataSource;


    public DataSourceTransactionManager dataSourceTransactionManager()
    {
        return new DataSourceTransactionManager(dataSource);
    }



}
