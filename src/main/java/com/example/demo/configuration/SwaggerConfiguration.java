package com.example.demo.configuration;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.RequestHandler;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {






    @Bean
    public Docket api()
    {


        List<SecurityScheme> securitySchemeslist = new ArrayList<>();

        securitySchemeslist.add(new BasicAuth("basicAuth"));
        securitySchemeslist.add(new ApiKey("API","virak","virak"));

        return  new Docket(DocumentationType.SWAGGER_2)
                .select()
                //.apis(RequestHandlerSelectors.any())
                .apis(RequestHandlerSelectors.basePackage("com.example.demo.controller.restcontroller"))
                //.paths(PathSelectors.any())

                .paths(PathSelectors.ant("/api/v1/**"))
                .build()
                .securitySchemes(securitySchemeslist)
                .apiInfo(apiInfo());
    }



    private ApiInfo apiInfo()
    {
        List<VendorExtension> vendorExtensions = new ArrayList<>();
        Contact contact = new Contact("Phay thavirak","","phaythavirak@gmail.com");
        ApiInfo apiInfomation = new ApiInfo("BMS",
                "Phaythavirak",
                "Version 1.0",
                "Term of Service",
                contact,
                "Licence of Right",
                "http://www.BMS.com",
                vendorExtensions
                );
        return apiInfomation;
    }
}
