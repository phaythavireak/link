package com.example.demo.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@Order(1)
public class APIConfiguration extends WebSecurityConfigurerAdapter {


    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().withUser("API_USERNAME").password(passwordEncoder.encode("API_PASSWORD")).roles("API");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.logout().logoutRequestMatcher(new AntPathRequestMatcher("/logoutapi"));
        http.antMatcher("/api/**").authorizeRequests().anyRequest().hasAnyRole("API");




        http.httpBasic();
    }
}
