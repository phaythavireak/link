package com.example.demo.controller;

import com.example.demo.model.Book;
import com.example.demo.service.BookService;
import com.example.demo.service.UploadService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

@Controller
public class BookController {
    private BookService bookService;
    private UploadService uploadService;

    public BookController(BookService bookService, UploadService uploadService) {
        this.bookService = bookService;
        this.uploadService = uploadService;
    }

    @RequestMapping("/index")
    public String index(Model model)
    {
        List<Book> books= this.bookService.getall();
        model.addAttribute("booklist" , books);
        return "/book/index";
    }
   @RequestMapping("/view/{id}")
   public String view(@PathVariable("id") Integer id,Model model)
    {
        Book book=this.bookService.findone(id);
        model.addAttribute("book",book);

        return "book/view";
    }
    @RequestMapping("/edit/{id}")
    public String edit(@PathVariable("id") Integer id,Model model)
    {
        Book book=this.bookService.findone(id);
        model.addAttribute("book",book);
       return "books/update";
    }
    @PostMapping("/update/submit")
    public String update(@ModelAttribute Book book)
    {
        this.bookService.update(book);
        return "redirect:/index";
    }
    @GetMapping("remove/{id}")
    public String remove(@PathVariable Integer id)
    {
        this.bookService.remove(id);
        return "redirect:/index";
    }
    @RequestMapping("/create")
    public String create(Model model)
    {
        model.addAttribute("book",new Book());
        return "book/create";
    }
    @RequestMapping("/creates")
    public String creates(Model model)
    {
        model.addAttribute("book",new Book());
        return "books/create";
    }
    @PostMapping("create/submit")
    public String insert( Book book, MultipartFile file)
    {
        String filename  = this.uploadService.singleupload(file,null);

        book.setThumnail("/image/"+filename);
        this.bookService.create(book);
        return "redirect:/index";
    }
}

