package com.example.demo.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TestController {



    @RequestMapping("/admin")
    public String admin()
    {
        return "admin/admin";
    }
    @RequestMapping("/user")
    public String user()
    {
        return "admin/user";
    }
    @RequestMapping("/dba")
    public String dba()
    {
        return "admin/dba";
    }
}
