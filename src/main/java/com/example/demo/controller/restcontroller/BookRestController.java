package com.example.demo.controller.restcontroller;


import com.example.demo.model.Book;
import com.example.demo.service.BookService;
import com.example.demo.service.UploadService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.awt.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class BookRestController {

    private BookService bookService;
    @Autowired
    private UploadService uploadService;

    public BookRestController(BookService bookService) {
        this.bookService = bookService;
    }

    @RequestMapping(value = "/test",produces = {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE}, method = RequestMethod.GET)
    @ApiOperation(value = "Get All Book", authorizations = {@Authorization(value = "basicAuth")})
    public Map<String, Object> getall() {
        Map<String, Object> map = new HashMap<>();
        List<Book> books = this.bookService.getall();
        if (!books.isEmpty()) {
            map.put("Status", true);
            map.put("Alert", "List All Book");
            map.put("Data", books);
        } else {
            map.put("Status", false);
            map.put("Data", "Not Found");

        }
        return map;
    }

    @PostMapping("/save")
    public Map<String, Object> save(@RequestBody Book book, MultipartFile file) {
        Map<String, Object> map = new HashMap<>();


        this.bookService.create(book);

        if (book != null) {
            map.put("Status", true);
            map.put("Alert", "All Book Inserted");
            map.put("Data", book);
        } else {
            map.put("Status", false);
            map.put("Alert", "Not Found");
        }
        return map;
    }

    @RequestMapping(value = "/delete/{id}")
    public Map<String, Object> delete(@PathVariable Integer id) {
        Map<String, Object> map = new HashMap<>();
        this.bookService.remove(id);
        map.put("Status", true);
        map.put("Alert", "Data has Been Deleted");

        return map;
    }

    @PutMapping("/update")
    public Map<String, Object> update(@RequestBody Book book) {
        Map<String, Object> map = new HashMap<>();
        this.bookService.update(book);
        if (book != null) {
            map.put("Status", true);
            map.put("Alert", "All Book Inserted");
            map.put("Data", book);
        } else {
            map.put("Status", false);
            map.put("Alert", "Not Found");
        }
        return map;
    }

    @PostMapping("/upload")
    public Map<String, Object> upload(@RequestParam("file") MultipartFile file) {
        Map<String, Object> map = new HashMap<>();
        String filename = this.uploadService.upload(file);
        if (filename != null) {
            map.put("Status", true);
            map.put("Alert", "All Book Inserted");
            map.put("Data", filename);
        } else {
            map.put("Status", false);
            map.put("Alert", "Not Found");
        }
        return map;
    }


}
