package com.example.demo.model;

import org.springframework.security.core.GrantedAuthority;

import java.util.List;

public class Role implements GrantedAuthority {
    private int id;
    private String role;
    private List<User> user;

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", Role='" + role + '\'' +
                ", user=" + user +
                '}';
    }


    @Override
    public String getAuthority() {
        return "ROLE_" + role;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<User> getUser() {
        return user;
    }

    public void setUser(List<User> user) {
        this.user = user;
    }

    public Role() {

    }

    public Role(int id, String role, List<User> user) {

        this.id = id;
        this.role = role;
        this.user = user;
    }
}
