package com.example.demo.repository;

import com.example.demo.model.Book;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;


@Repository
public interface BookRepository{







    @Select("select * from books order by id")
    List<Book> getall();
    @Select("select * from books where id=#{id}")
    Book findone(Integer id);
    @Update("update books set title=#{title},author=#{author},publisher=#{publisher},thumnail=#{thumnail} where id = #{id}")
    boolean update(Book book);
    @Delete("Delete from books where id =#{id}")
    boolean remove(Integer id);
    @Insert("insert into books(title,author,publisher,thumnail) values(#{title},#{author},#{publisher},#{thumnail})")
    boolean create(Book book);


}
