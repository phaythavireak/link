package com.example.demo.repository;


import com.example.demo.model.Role;
import com.example.demo.model.User;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserReporsitory {



        @Select("select id,username,password,status,profile from users where username = #{username}")

        @Results({
                @Result(property = "id" , column = "id"),
                @Result(property = "profile",column = "profile"),
                @Result(property = "role",column = "id" ,many = @Many(select = "getRoleById"))
        })
        User loadUserByUsername(String username);




        @Select("SELECT * from role r INNER JOIN user_role ur on r.id = ur.role_id where ur.user_id=#{id}")
        List<Role> getRoleById(Integer id);




}
