package com.example.demo.service.imple;

import com.example.demo.model.User;
import com.example.demo.repository.UserReporsitory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service("ImpuserDetailsService")
public class ImpuserDetailsService implements UserDetailsService {

    @Autowired
    private UserReporsitory userReporsitory;
    public ImpuserDetailsService(UserReporsitory userReporsitory) {
        this.userReporsitory = userReporsitory;
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
       User user = this.userReporsitory.loadUserByUsername(username);
        return user;

    }

}
