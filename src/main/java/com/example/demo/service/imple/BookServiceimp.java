package com.example.demo.service.imple;

import com.example.demo.model.Book;
import com.example.demo.repository.BookRepository;
import com.example.demo.service.BookService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceimp implements BookService {


    private BookRepository bookRepository;

    public BookServiceimp(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public List<Book> getall() {
        return this.bookRepository.getall();
    }

    @Override
    public boolean create(Book book) {
        return this.bookRepository.create(book);
    }

    @Override
    public boolean remove(Integer id) {
        return this.bookRepository.remove(id);

    }

    @Override
    public boolean update(Book book) {
        return this.bookRepository.update(book);
    }

    @Override
    public Book findone(Integer id) {
        return this.bookRepository.findone(id);
    }
}
