package com.example.demo.service.imple;

import com.example.demo.service.UploadService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class UploadServiceImp implements UploadService{


    @Override
    public String singleupload(MultipartFile file, String folder) {
        if(file.isEmpty())
            return null;
        if (folder == null)
            folder = "";
        File path=new File("/Users/sbc/Documents/connect_postgres/image/"+folder);
        if(!path.exists())
            path.mkdir();
        String filename = file.getOriginalFilename();
        String extension = filename.substring(filename.lastIndexOf('.')+1);
        filename = UUID.randomUUID()+"."+extension;
        try{
            Files.copy(file.getInputStream(),Paths.get("/Users/sbc/Documents/connect_postgres/image/"+folder,filename));
        }catch (IOException e)
        {
            System.out.println("erorr");
        }


        return folder+filename;
    }

    @Override
    public String upload(MultipartFile file) {
        if(file.isEmpty())
            return null;
        File path=new File("/Users/sbc/Documents/connect_postgres/image/");
        if(!path.exists())
            path.mkdir();
        String filename = file.getOriginalFilename();
        String extension = filename.substring(filename.lastIndexOf('.')+1);
        filename = UUID.randomUUID()+"."+extension;
        try{
            Files.copy(file.getInputStream(),Paths.get("/Users/sbc/Documents/connect_postgres/image/",filename));
        }catch (IOException e)
        {
            System.out.println("erorr");
        }


        return filename;
    }

    @Override
    public List<String> multiupload(List<MultipartFile> files, String folder) {
        List<String> filenames = new ArrayList<>();

        files.forEach(file -> {
            filenames.add(this.singleupload(file, folder));
        });

        return filenames;
    }
}
