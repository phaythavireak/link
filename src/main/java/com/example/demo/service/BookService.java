package com.example.demo.service;

import com.example.demo.model.Book;

import java.util.List;

public interface BookService {
    List<Book> getall();
    Book findone(Integer id);
    boolean update(Book book);
    boolean remove(Integer id);
    boolean create(Book book);

}
