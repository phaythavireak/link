package com.example.demo.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface UploadService {


     String singleupload(MultipartFile file, String folder);
     String upload(MultipartFile file);
     List<String> multiupload(List<MultipartFile> files,String folder);

}
